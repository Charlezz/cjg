package com.charlezz.threadtest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    private Button start;
    private Button start2;
    private TextView count;

    private Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            count.setText("" + msg.what);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        start = (Button) findViewById(R.id.start);
        start2 = (Button) findViewById(R.id.start2);
        count = (TextView) findViewById(R.id.count);

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Thread t1 = new Thread(new Runnable() {
                    int number = 0;

                    @Override
                    public void run() {
                        for (int i = 0; i < 1000000; i++) {
                            //액티비티에서만 할 수 있는 방법, 자식스레드가 메인쓰레드에게 이벤트 전달하기
                            number++;
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    count.setText(String.valueOf(number));
                                }
                            });
                        }
                    }
                });
                t1.start();
            }
        });


        start2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread t1 = new Thread(new Runnable() {
                    int number = 0;

                    @Override
                    public void run() {
                        for (int i = 0; i < 1000000; i++) {
                            number++;
                            mHandler.sendEmptyMessage(number);
                        }
                    }
                });
                t1.start();
            }
        });
    }
}
