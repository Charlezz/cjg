package com.example;

import java.util.concurrent.Semaphore;

public class Main {

    private static int count = 0;
    private static Object lock = new Object();

    private static Semaphore semaphore = new Semaphore(1);


    public static void main(String[] args) {
        System.out.println("Start");

//        Thread t1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 10000000; i++) {
//                    synchronized (lock) {
//                        count++;
//                    }
//                }
//
//            }
//        });
//
//        Thread t2 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                for (int i = 0; i < 10000000; i++) {
//                    synchronized (lock) {
//                        count--;
//                    }
//                }
//            }
//        });

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        semaphore.acquire();
                        count++;
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10000000; i++) {
                    try {
                        semaphore.acquire();
                        count--;
                        semaphore.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });


        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("count:" + count);
        System.out.println("Finished");
    }
}
