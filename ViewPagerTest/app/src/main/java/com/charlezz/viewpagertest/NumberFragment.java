package com.charlezz.viewpagertest;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Charles on 2017. 4. 30..
 */

public class NumberFragment extends Fragment {

    private static final String KEY_NUMBER = "number";

    public static NumberFragment newInstance(int num) {

        Bundle args = new Bundle();
        args.putInt(KEY_NUMBER, num);
        NumberFragment fragment = new NumberFragment();
        fragment.setArguments(args);
        return fragment;
    }


    private int number;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        number = getArguments().getInt(KEY_NUMBER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.number_fragment_layout, container, false);
        TextView tv = (TextView) view.findViewById(R.id.number);
        tv.setText(String.valueOf(number));
        return view;
    }
}
