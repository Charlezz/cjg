package com.charlezz.phonecallreceivetest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Charles on 2017. 2. 26..
 */

public class PhonCallReceiver extends BroadcastReceiver {

    public static final String TAG = PhonCallReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive");

        String action = intent.getAction();
        String number = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);


        if (!TextUtils.isEmpty(number)) {
            Log.e(TAG, "number:" + number);
        }


        switch (action) {
            case TelephonyManager.ACTION_PHONE_STATE_CHANGED:
                Log.e(TAG, "ACTION_PHONE_STATE_CHANGED");
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);



                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    Log.e(TAG, "EXTRA_STATE_RINGING");
                } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.e(TAG, "EXTRA_STATE_IDLE");
                } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.e(TAG, "EXTRA_STATE_OFFHOOK");
                }
                break;
        }
    }
}
