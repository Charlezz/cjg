package com.charlezz.intentpractice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    EditText input;
    Button pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        input = (EditText) findViewById(R.id.inputText);
        pass = (Button) findViewById(R.id.pass);

        pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = input.getText().toString();

                Intent intent = new Intent();
                intent.putExtra("message", value);
                intent.setClass(MainActivity.this, SecondActivity.class);

//                startActivity(intent);

                startActivityForResult(intent, 0);

//                Toast.makeText(MainActivity.this, "Clicked:" + value, Toast.LENGTH_SHORT).show();


            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0 && resultCode == Activity.RESULT_OK) {
            Log.e(TAG, "onActivityResult");
            String reply = data.getStringExtra("result");
            Toast.makeText(MainActivity.this, reply, Toast.LENGTH_SHORT).show();

        } else {
            Log.e(TAG, "failed");
        }


    }
}
