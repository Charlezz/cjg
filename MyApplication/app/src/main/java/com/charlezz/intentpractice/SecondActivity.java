package com.charlezz.intentpractice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    TextView receivedText;

    EditText replyText;
    Button reply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setResult(Activity.RESULT_CANCELED);

        setContentView(R.layout.activity_second);
        receivedText = (TextView) findViewById(R.id.receivedText);

        Intent intent = getIntent();
        String value = intent.getStringExtra("message");
        receivedText.setText(value);


        replyText = (EditText) findViewById(R.id.replyText);
        reply = (Button) findViewById(R.id.reply);

        reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = replyText.getText().toString();
                Intent resultIntent = new Intent(SecondActivity.this, MainActivity.class);
                resultIntent.putExtra("result", result);
                setResult(Activity.RESULT_OK, resultIntent);

                finish();
            }
        });

    }
}
