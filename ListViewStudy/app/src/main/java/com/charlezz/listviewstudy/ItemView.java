package com.charlezz.listviewstudy;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Charles on 2017. 4. 9..
 */

public class ItemView extends FrameLayout {

    private TextView titleView;
    private ImageView iconView;
    private Button button;
    private ItemData data;
    private Context context;

    public ItemView(@NonNull Context context) {
        super(context);
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.item_view, this, true);

        iconView = (ImageView) view.findViewById(R.id.icon);
        titleView = (TextView) view.findViewById(R.id.title);
        button = (Button) view.findViewById(R.id.btn);

        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(ItemView.this.context, data.name, Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setData(ItemData data) {
        this.data = data;
        titleView.setText(data.name);
        iconView.setImageResource(data.icon_id);
    }
}
