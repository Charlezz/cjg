package com.charlezz.listviewstudy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    String[] data = new String[]{"Alpha", "Beta", "Cupcake", "Donut", "Eclaire", "Froyo", "Ginger Bread", "Honeycomb", "Icecream Sandwitch"
            , "Jelly bean", "Kitkat", "Lollipop", "Marshmellow", "Nougat"};

    int[] iconIds = new int[]{
            R.drawable.alpha,
            R.drawable.beta,
            R.drawable.cupcake,
            R.drawable.donut,
            R.drawable.eclaire,
            R.drawable.froyo,
            R.drawable.gingerbread,
            R.drawable.honeycomb,
            R.drawable.icecreamsandwitch,
            R.drawable.jellybean,
            R.drawable.kitkat,
            R.drawable.lollipop,
            R.drawable.marshmellow,
            R.drawable.nougat
    };


    ListView listView;
    AndroidAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create data
        ArrayList<ItemData> items = new ArrayList<>();

        for (int i = 0; i < data.length; i++) {
            ItemData itemData = new ItemData();
            itemData.id = i;
            itemData.name = data[i];
            itemData.icon_id = iconIds[i];
            items.add(itemData);
        }

        // listView setting
        listView = (ListView) findViewById(R.id.listView);
        // generate adapter
        adapter = new AndroidAdapter();
        listView.setAdapter(adapter);
        adapter.setItems(items);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, ((ItemData) adapter.getItem(position)).name, Toast.LENGTH_SHORT).show();
            }
        });

    }

}
