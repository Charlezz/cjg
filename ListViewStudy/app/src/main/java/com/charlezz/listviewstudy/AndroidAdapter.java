package com.charlezz.listviewstudy;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 4. 9..
 */

public class AndroidAdapter extends BaseAdapter {

    private ArrayList<ItemData> items = new ArrayList<>();

    public void setItems(ArrayList<ItemData> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = new ItemView(parent.getContext());
        }

        ((ItemView) convertView).setData(items.get(position));

        return convertView;
    }
}
