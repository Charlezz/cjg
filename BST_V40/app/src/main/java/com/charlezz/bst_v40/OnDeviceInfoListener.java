package com.charlezz.bst_v40;

/**
 * Created by Charles on 2/27/17.
 */

public interface OnDeviceInfoListener {

    void onReadRssi(int rssi);

    void onBatterChanged(int level);

    void OnConnectionChanged(int state, String deviceName);

}
