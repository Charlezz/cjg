package com.charlezz.bst_v40;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Charles on 2/27/17.
 */
public class PrefManager {

    public static String TAG = PrefManager.class.getSimpleName();

    private static PrefManager ourInstance = new PrefManager();

    public static PrefManager getInstance() {
        return ourInstance;
    }

    private SharedPreferences.Editor editor;
    private SharedPreferences pref;

    public static final String KEY_DEVICE_ADDRESS = "device address";


    private PrefManager() {
//        MyApp.getContext().getSharedPreferences("prefs",)
        pref = PreferenceManager.getDefaultSharedPreferences(MyApp.getContext());
        editor = pref.edit();
    }

    public void setAddress(String address) {
        editor.putString(KEY_DEVICE_ADDRESS, address);
        editor.commit();
    }

    public String getAddress() {
        return pref.getString(KEY_DEVICE_ADDRESS, "");
    }
}
