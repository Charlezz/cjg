package com.charlezz.bst_v40;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;

public class ScanActivity extends AppCompatActivity {

    public static final String TAG = ScanActivity.class.getSimpleName();

    private static final int SCAN_DURATION = 5000;
    private static final int MENU_DEFAULT_GROUP = 0;
    private static final int MENU_SCAN = 0;


    public static final String DEVICE_NAME = "device_name";
    public static final String DEVICE_ADDRESS = "device_address";


    @BindView(R.id.listView)
    ListView listView;
    LeDeviceListAdapter mAdapter;

    public BluetoothAdapter mBluetoothAdapter;
    public BluetoothManager mBluetoothManager;

    private Unbinder unbinder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        setResult(RESULT_CANCELED);
        unbinder = ButterKnife.bind(this);
        mAdapter = new LeDeviceListAdapter(this);
        listView.setAdapter(mAdapter);

        mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = mBluetoothManager.getAdapter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem scanItem = menu.add(MENU_DEFAULT_GROUP, MENU_SCAN, MENU_SCAN, "scan");
        scanItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_SCAN:
                startScan();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startScan();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
    }

    public void startScan() {
        Log.e(TAG, "startScan");
        mAdapter.clear();
        mBluetoothAdapter.getBluetoothLeScanner().startScan(mScanCallback);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScan();
            }
        }, SCAN_DURATION);
    }

    public void stopScan() {
        Log.e(TAG, "stopScan");
        mBluetoothAdapter.getBluetoothLeScanner().stopScan(mScanCallback);
    }

    private ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);

            BluetoothDevice device = result.getDevice();
            Log.e(TAG, "onScanResult:" + device.getAddress());
            mAdapter.addDevice(device);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            Log.e(TAG, "onBatchScanResults");
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e(TAG, "onScanFailed");
        }
    };

    @OnItemClick(R.id.listView)
    void onItemClicked(int position) {
        BluetoothDevice device = mAdapter.getItem(position);

        String name = device.getName();
        String address = device.getAddress();

        Intent intent = new Intent();
        intent.putExtra(DEVICE_NAME, name);
        intent.putExtra(DEVICE_ADDRESS, address);

        setResult(RESULT_OK, intent);
        finish();

    }
}
