package com.charlezz.bst_v40;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;

import static com.charlezz.bst_v40.Constant.ACTION_BST_PHONE_STATE;
import static com.charlezz.bst_v40.Constant.ACTION_BST_PHONE_STATE_CHANGED;

/**
 * Created by Charles on 2017. 3. 5..
 */

public class BSTReceiver extends BroadcastReceiver {

    public static final String TAG = BSTReceiver.class.getSimpleName();


    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e(TAG, "onReceive");
        String action = intent.getAction();
        switch (action) {
            case TelephonyManager.ACTION_PHONE_STATE_CHANGED:
                Log.e(TAG, "ACTION_PHONE_STATE_CHANGED");
                String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

                //re-broadcast
                Intent callStateIntent = new Intent(ACTION_BST_PHONE_STATE_CHANGED);
                callStateIntent.putExtra(ACTION_BST_PHONE_STATE, state);
                context.sendBroadcast(callStateIntent);

                if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                    Log.e(TAG, "EXTRA_STATE_RINGING");
                } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Log.e(TAG, "EXTRA_STATE_IDLE");
                } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Log.e(TAG, "EXTRA_STATE_OFFHOOK");
                }
                break;

            case Constant.ACTION_BST_DISCONNECTED:
                Intent alarmIntent = new Intent(context, AlarmActivity.class);
                alarmIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(alarmIntent);
                break;
        }
    }
}
