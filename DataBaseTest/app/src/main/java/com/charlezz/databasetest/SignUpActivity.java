package com.charlezz.databasetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import io.realm.Realm;

public class SignUpActivity extends AppCompatActivity {

    EditText id, pw, name, age;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        id = (EditText) findViewById(R.id.id);
        pw = (EditText) findViewById(R.id.pw);
        name = (EditText) findViewById(R.id.name);
        age = (EditText) findViewById(R.id.age);

        save = (Button) findViewById(R.id.save);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Realm realm = Realm.getDefaultInstance();


                User user = new User(
                        id.getText().toString(),
                        pw.getText().toString(),
                        name.getText().toString(),
                        Integer.parseInt(age.getText().toString())
                );


                realm.beginTransaction();//트랜잭션 시작
                realm.copyToRealmOrUpdate(user);// user 데이터 저장
                realm.commitTransaction();//트랜잭션 끝

                finish();
            }
        });


        if (getIntent().hasExtra("update")) {
            User user = getIntent().getParcelableExtra("update");
            id.setText(user.getId());
            pw.setText(user.getPw());
            name.setText(user.getName());
            age.setText(String.valueOf(user.getAge()));

        }
    }


}
