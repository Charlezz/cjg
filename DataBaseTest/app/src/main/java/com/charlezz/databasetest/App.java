package com.charlezz.databasetest;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by Charles on 23/07/2017.
 */

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //애플리케이션이 시작될 때 가장먼저 시작되는 지점

        Realm.init(this);

    }
}
