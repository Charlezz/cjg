package com.charlezz.databasetest;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Charles on 23/07/2017.
 */

public class UserAdapter extends BaseAdapter {

    private RealmResults<User> users;
    private Realm realm;


    private RealmChangeListener<RealmResults<User>> realmChangeListener = new RealmChangeListener<RealmResults<User>>() {
        @Override
        public void onChange(RealmResults<User> element) {
            users = element;
            notifyDataSetChanged();
        }
    };

    public UserAdapter() {
        realm = Realm.getDefaultInstance();
        users = realm.where(User.class).findAll();

        users.addChangeListener(realmChangeListener);

    }

    @Override
    public int getCount() {
        return users.size();
    }

    @Override
    public User getItem(int i) {
        return users.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, final ViewGroup viewGroup) {
        UserView userView = (UserView) view;

        if (userView == null) {
            userView = new UserView(viewGroup.getContext());
        }
        userView.setUser(getItem(i));

        userView.setOnUserViewButtonClickListener(new UserView.OnUserViewButtonClickListener() {
            @Override
            public void onUpdate() {
                //MVC위반
                Intent intent = new Intent(viewGroup.getContext(), SignUpActivity.class);
                intent.putExtra("update", users.get(i));

                viewGroup.getContext().startActivity(intent);
            }

            @Override
            public void onDelete() {
                realm.beginTransaction();
                users.get(i).deleteFromRealm();
                realm.commitTransaction();
                notifyDataSetChanged();
            }
        });

        return userView;
    }


}
