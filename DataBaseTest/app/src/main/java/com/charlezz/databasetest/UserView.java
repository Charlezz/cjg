package com.charlezz.databasetest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Charles on 23/07/2017.
 */

public class UserView extends FrameLayout {

    private TextView id;
    private TextView name;
    private TextView pw;
    private TextView age;

    private ImageButton update, delete;

    public UserView(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.user_view, this, true);
        id = view.findViewById(R.id.id);
        name = view.findViewById(R.id.name);
        pw = view.findViewById(R.id.pw);
        age = view.findViewById(R.id.age);

        update = view.findViewById(R.id.update);
        delete = view.findViewById(R.id.delete);

        update.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onUserViewButtonClickListener != null) {
                    onUserViewButtonClickListener.onUpdate();
                }
            }
        });

        delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onUserViewButtonClickListener != null) {
                    onUserViewButtonClickListener.onDelete();
                }
            }
        });
    }

    public void setUser(User user) {
        id.setText(user.getId());
        name.setText(user.getName());
        pw.setText(user.getPw());
        age.setText(String.valueOf(user.getAge()));
    }


    private OnUserViewButtonClickListener onUserViewButtonClickListener;

    public void setOnUserViewButtonClickListener(OnUserViewButtonClickListener onUserViewButtonClickListener) {
        this.onUserViewButtonClickListener = onUserViewButtonClickListener;
    }

    public interface OnUserViewButtonClickListener {
        void onUpdate();

        void onDelete();
    }
}
