package com.charlezz.seekbartest;

import android.Manifest;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.locks.ReentrantLock;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    TextView tv;
    SeekBar seekBar;
    Button startBtn;

    TextView duration, currentPosition;


    private ReentrantLock lock = new ReentrantLock();

    MediaPlayer mediaPlayer;


    Thread mediaPlayerWatcher = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    if (!lock.isLocked()) {
                        int t = mediaPlayer.getCurrentPosition();
                        handler.sendEmptyMessage(t);
                    }
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    });

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int t = msg.what;
            seekBar.setProgress(t);
            currentPosition.setText(getTimeFormat(t));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.tv);
        seekBar = (SeekBar) findViewById(R.id.seek_bar);
        startBtn = (Button) findViewById(R.id.start);
        duration = (TextView) findViewById(R.id.duration);
        currentPosition = (TextView) findViewById(R.id.current_position);

        new TedPermission(this)
                .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        MediaPlayer mp = loadMusicFromAsset();
                        if (mp == null) {
                            Log.e(TAG, "에러, 파일 로드 실패");
                            return;
                        }
                        mediaPlayer = mp;
                        seekBar.setMax(mp.getDuration());
                        Log.e(TAG, "mp.getDuration():" + mp.getDuration());
                        duration.setText(getTimeFormat(mp.getDuration()));


                    }

                    @Override
                    public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                        finish();
                    }
                })
                .check();


        startBtn.setOnClickListener(startClickListener);

        seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);

    }

    View.OnClickListener startClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mediaPlayer.start();
            mediaPlayerWatcher.start();
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            tv.setText("Value:" + i);
            currentPosition.setText(getTimeFormat(i));
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            lock.lock();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            int progress = seekBar.getProgress();
            mediaPlayer.seekTo(progress);
            lock.unlock();
        }
    };

    private MediaPlayer loadMusicFromAsset() {
        AssetManager am = getAssets();
        try {
            AssetFileDescriptor afd = am.openFd("twice_signal.mp3");
            MediaPlayer mp = new MediaPlayer();
            mp.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mp.prepare();
            return mp;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getTimeFormat(int t) {
        int m = t / 1000 / 60;
        int s = (t / 1000) % 60;
        return String.format("%02d:%02d", m, s);
    }

}
