package com.charlezz.fragmentpractice;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Charles on 2017. 4. 23..
 */

public class NumberFragment extends Fragment {

    private static final String KEY_NUMBER = "number";

    private int number;

    TextView tv;

    //프레그먼트는 언제든지 기본생성자를 통해 재생성되는데, 그때 인자(Argument) 전달을 안정적으로 하기위해
    //팩토리 패턴(Factory Pattern)을 쓴다.
    public static NumberFragment newInstance(int num) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_NUMBER, num);
        NumberFragment fragment = new NumberFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    //기본 Constructor(생성자)
    public NumberFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle b = getArguments();
        number = b.getInt(KEY_NUMBER);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //fragment가 눈에 보이려면 View를 리턴시켜야함
        //이곳에서 뷰를 만들어서 또는 xml파일로부터 인플레이팅 하는 과정을 거쳐서 View를 리턴 시키면 눈에보인다.
        View view = inflater.inflate(R.layout.number_fragment_layout, container, false);
        tv = (TextView) view.findViewById(R.id.tv);
        tv.setText("Hello World");
        tv.setText(String.valueOf(number));
        return view;
    }
}
