package com.charlezz.fragmentpractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = MainActivity.class.getSimpleName();
    Button btn1, btn2, btn3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Button 1 Clicked");
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, NumberFragment.newInstance(1))
                        .commit();
            }
        });

        btn2.setOnClickListener(this);
        btn3.setOnClickListener(listener);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, ImageFragment.newInstance())
                .commit();


    }

    @Override
    public void onClick(View v) {
        Log.e(TAG, "Button 2 Clicked");
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, NumberFragment.newInstance(2))
                .commit();
    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.e(TAG, "Button 3 Clicked");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, ImageFragment.newInstance())
                    .commit();
        }
    };
}
