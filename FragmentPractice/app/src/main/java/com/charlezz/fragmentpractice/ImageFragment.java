package com.charlezz.fragmentpractice;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Charles on 2017. 4. 23..
 */

public class ImageFragment extends Fragment {

    ImageView iv;
    TextView tv;

    public static ImageFragment newInstance() {
        ImageFragment fragment = new ImageFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.image_fragment_layout, container, false);
        iv = (ImageView) v.findViewById(R.id.image);
        tv = (TextView) v.findViewById(R.id.tv);
        iv.setImageResource(R.drawable.iu);
        tv.setText("아이유");
        return v;
    }
}
