package com.charlezz.servicedemo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();
    Button startService1, stopService1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startService1 = (Button) findViewById(R.id.startService1);
        startService1.setOnClickListener(this);
        stopService1 = (Button) findViewById(R.id.stopService1);
        stopService1.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startService1:
                Intent serviceIntent = new Intent(MainActivity.this, MyService.class);
                serviceIntent.putExtra("test", 100);
                startService(serviceIntent);
                break;
            case R.id.stopService1:
                Intent serviceStopIntent = new Intent(MainActivity.this, MyService.class);
                stopService(serviceStopIntent);
                break;
        }

    }
}
