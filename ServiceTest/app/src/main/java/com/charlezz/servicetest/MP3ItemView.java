package com.charlezz.servicetest;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

/**
 * Created by Charles on 2017. 5. 21..
 */

public class MP3ItemView extends FrameLayout {

    ImageView thumbnail;
    TextView songName;
    TextView artistName;

    public MP3ItemView(@NonNull Context context) {
        super(context);
        View view = LayoutInflater.from(context).inflate(R.layout.mp3_view_layout, this, true);
        thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
        songName = (TextView) view.findViewById(R.id.song_name);
        artistName = (TextView) view.findViewById(R.id.artist_name);
    }

    private MP3Item mMP3Item;

    public void setMP3Item(MP3Item item) {
        this.mMP3Item = item;
        songName.setText(item.songName);
        artistName.setText(item.artist);
        if (!TextUtils.isEmpty(item.thumbnail)) {
            thumbnail.setImageURI(Uri.fromFile(new File(item.thumbnail)));
        }

    }
}
