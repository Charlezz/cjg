package com.charlezz.servicetest;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by Charles on 2017. 5. 21..
 */

public class MP3Adapter extends BaseAdapter {

    private ArrayList<MP3Item> items = new ArrayList<>();

    public void setItems(ArrayList<MP3Item> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public MP3Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        MP3ItemView view = (MP3ItemView) convertView;

        if (convertView == null) {
            view = new MP3ItemView(parent.getContext());
        }

        view.setMP3Item(getItem(position));
        return view;
    }
}
