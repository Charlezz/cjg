package com.charlezz.servicetest;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Charles on 2017. 5. 21..
 */

public class MP3Item implements Parcelable {
    public long id;
    public String thumbnail;
    public String songName;
    public String artist;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.thumbnail);
        dest.writeString(this.songName);
        dest.writeString(this.artist);
    }

    public MP3Item() {
    }

    protected MP3Item(Parcel in) {
        this.id = in.readLong();
        this.thumbnail = in.readString();
        this.songName = in.readString();
        this.artist = in.readString();
    }

    public static final Parcelable.Creator<MP3Item> CREATOR = new Parcelable.Creator<MP3Item>() {
        @Override
        public MP3Item createFromParcel(Parcel source) {
            return new MP3Item(source);
        }

        @Override
        public MP3Item[] newArray(int size) {
            return new MP3Item[size];
        }
    };
}
