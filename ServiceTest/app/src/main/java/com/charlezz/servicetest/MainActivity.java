package com.charlezz.servicetest;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String TAG = MainActivity.class.getSimpleName();
    ListView listView;

    MP3Adapter adapter;

    ImageButton previous, playOrPause, next;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new MP3Adapter();
        listView.setAdapter(adapter);

        previous = (ImageButton) findViewById(R.id.previous);
        playOrPause = (ImageButton) findViewById(R.id.play_or_pause);
        next = (ImageButton) findViewById(R.id.next);
        previous.setOnClickListener(this);
        playOrPause.setOnClickListener(this);
        next.setOnClickListener(this);


        //mp3item이 저장될 리스트
        ArrayList<MP3Item> items = new ArrayList<>();

        //폰에 있는 mp3 list 가져오는 방법
        //리졸버란 컨텐츠를 가져오는 일을수행한다

        Cursor cursor = getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                null,
                null,
                null,
                null
        );

        while (cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media._ID));
            String songName = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME));
            String artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
            long albumId = cursor.getLong(cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID));

            MP3Item item = new MP3Item();
            //썸네일 얻기

            Cursor thumbnailCursor = getContentResolver().query(
                    MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                    new String[]{MediaStore.Audio.Albums.ALBUM_ART},
                    MediaStore.Audio.Albums._ID + "=?",
                    new String[]{String.valueOf(albumId)},
                    null
            );
            if (thumbnailCursor.moveToNext()) {
                //앨범아이디로 조회한 값이 존재한다면 이곳으로
                String thumbnailPath = thumbnailCursor.getString(thumbnailCursor.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART));
                Log.e(TAG, "thumbnailPath:" + thumbnailPath);
                if (!TextUtils.isEmpty(thumbnailPath)) {
                    item.thumbnail = thumbnailPath;
                }
            }
            item.id = id;
            item.songName = songName;
            item.artist = artist;
            items.add(item);
        }
        adapter.setItems(items);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MainActivity.this, MP3Service.class);
                intent.putExtra(MP3Service.KEY_MP3ITEM, adapter.getItem(position));
                startService(intent);

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.previous:
                Toast.makeText(MainActivity.this, "previous", Toast.LENGTH_SHORT).show();
                break;
            case R.id.play_or_pause:
                Toast.makeText(MainActivity.this, "play", Toast.LENGTH_SHORT).show();
                break;
            case R.id.next:
                Toast.makeText(MainActivity.this, "next", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
