package com.charlezz.servicetest;

import android.app.Notification;
import android.app.Service;
import android.content.ContentUris;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;

public class MP3Service extends Service {
    public static final String TAG = MP3Service.class.getSimpleName();

    public static final String KEY_MP3ITEM = "mp3item";

    private MediaPlayer mediaPlayer = null;

    public MP3Service() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(TAG, "onCreate");


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");

        if (intent.hasExtra(KEY_MP3ITEM)) {
            MP3Item item = intent.getParcelableExtra(KEY_MP3ITEM);

            if (mediaPlayer != null) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = null;
            }

            mediaPlayer = new MediaPlayer();
            try {
                Uri uri = ContentUris.withAppendedId(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, item.id);
                mediaPlayer.setDataSource(MP3Service.this, uri);
                mediaPlayer.prepare();
                mediaPlayer.start();

                Notification noti = new NotificationCompat.Builder(this)
                        .setContentTitle(item.songName)
                        .setContentText(item.artist)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setLargeIcon(BitmapFactory.decodeFile(item.thumbnail))
                        .build();

                startForeground(1, noti);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
    }
}
