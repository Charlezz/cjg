package com.charlezz.listviewpractice;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Charles on 2017. 4. 2..
 */

public class DessertAdapter extends BaseAdapter {

    private ArrayList<String> items = new ArrayList<>();

    public void setItems(String[] items) {
        Collections.addAll(this.items, items);
        notifyDataSetChanged();
    }

    public void add(String item) {
        items.add(item);
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv = null;

        if (convertView == null) {
            tv = new TextView(parent.getContext());
            tv.setPadding(30, 30, 30, 30);
        } else {
            tv = (TextView) convertView;
        }
        tv.setText(items.get(position));

        return tv;
    }
}
