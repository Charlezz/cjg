package com.charlezz.listviewpractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listView;

    String[] data = new String[]{"Alpha", "Beta", "Cupcake", "Donut", "Eclaire", "Froyo", "Ginger Bread", "Honeycomb", "Icecream Sandwitch"
            , "Jelly bean", "Kitkat", "Lollipop", "Marshmellow", "Nougat"};

    DessertAdapter adapter;

    EditText editText;
    Button add;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.listView);
        adapter = new DessertAdapter();

        listView.setAdapter(adapter);
        adapter.setItems(data);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this, (CharSequence) adapter.getItem(position), Toast.LENGTH_SHORT).show();
            }
        });

        editText = (EditText) findViewById(R.id.editText);
        add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.add(editText.getText().toString());
            }
        });


    }
}
