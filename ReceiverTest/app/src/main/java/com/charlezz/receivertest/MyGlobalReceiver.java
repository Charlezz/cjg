package com.charlezz.receivertest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Charles on 2017. 4. 30..
 */

public class MyGlobalReceiver extends BroadcastReceiver {
    public static final String TAG = MyGlobalReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        switch (action) {
            case Intent.ACTION_BATTERY_OKAY:
                Log.e(TAG, "ACTION_BATTERY_OKAY");
                break;
            case Intent.ACTION_BATTERY_LOW:
                Log.e(TAG, "ACTION_BATTERY_LOW");
                break;
            case Intent.ACTION_BOOT_COMPLETED:
                Log.e(TAG, "ACTION_BOOT_COMPLETED");
                break;
        }
    }
}
